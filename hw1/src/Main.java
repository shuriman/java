package com.company;

import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args){

        mattrix();

//        printThreeWords();
//
//        checkSumSign();
//
//        printColor();
//
//        compareNumbers();
    }

    private static void printThreeWords()
    {
        String[] fruits = {"Orange", "Banana", "Apple"};

        for (String fruit : fruits) {
            System.out.println(fruit);
        }
    }

    private static void checkSumSign()
    {
        int a =  ThreadLocalRandom.current().nextInt(1, 10 + 1), b =ThreadLocalRandom.current().nextInt(1, 10 + 1);


        if((a + b) >= 0){
            System.out.println("Сумма положительная");
        }else{
            System.out.println("Сумма отрицательная");
        }
    }

    private static  void printColor()
    {
        int value = ThreadLocalRandom.current().nextInt(0, 100 + 1);

        System.out.println(value);

        if(value < 0){
            System.out.println("Красный");
        }
        else if(value >= 0  && value < 100){
            System.out.println("Желтый");
        }
        else{
            System.out.println("Зеленый");
        }
    }

    private static void compareNumbers()
    {
        int a =  ThreadLocalRandom.current().nextInt(1, 10 + 1), b =ThreadLocalRandom.current().nextInt(1, 10 + 1);

        if(a >= b){
            System.out.println("a >= b");
        }else{
            System.out.println("a < b");
        }
    }


    //----- HomeWork 2 ----//
    private static boolean compareValue(int a, int b)
    {
        if( (a + b) >= 10 && (a + b) <= 20){
            return true;
        }

        return false;
    }

    private static boolean isPositive(int a){

        if(a >= 0){
            System.out.println("Положительное");
            return true;
        }
        System.out.println("Отрицательное");
        return false;
    }

    private static void printRepeatDig(int count, String s)
    {

        for (int i = 0;i<count;i++)
        {
            System.out.println(s);
        }
    }

    private static boolean checkYear(int year){
        if(year % 4 == 0){
            if(year % 100 == 0){
                if(year % 400 == 0){
                    return true;
                }
                return false;
            }
            return true;
        }
        return false;
    }
    //----- HomeWork 3 ----//

    private static void mattrix(){

        int[] arr = { 1, 1, 0, 0, 1, 0, 1, 1, 0, 0 };

        for(int i = 0; i < arr.length; i++){
            if(arr[i] == 1)
                arr[i] = 0;
            arr[i] = 1;
        }

        int[] intArr = new int[100];

        for(int i = 0; i < arr.length; i++){
            arr[i] = i + 1;
        }

        int[] arr2 = { 1, 5, 3, 2, 11, 4, 5, 2, 4, 8, 9, 1 };
        for(int i = 0; i < arr2.length; i++) {
            if(arr2[i] < 6){
                arr2[i] = arr2[i] * 2;
            }
        }

        int x = 10;
        int y = 10;
        int arr3[][] = new int[x][y];

        for(int i = 0; i < x; i++) {
            for(int j = 0; j < y; j++) {
                if(i == j || (j == y - i - 1))
                    arr3[i][j] = 1;
                System.out.print(arr3[i][j] + " ");
            }
            System.out.println(" ");
        }

        int[] arrMinMax = { 1, 5, 3, 2, 11, 4, 5, 2, 4, 8, 9, 1 };

        int min = 100 , max = 0;

        for(int i = 0; i < arrMinMax.length; i++) {
            if(max < arrMinMax[i]){
                max = arrMinMax[i];
            }

            if(min > arrMinMax[i]){
                min = arrMinMax[i];
            }
        }

        checkBalance();
    }

    private static int[] getMatrix(int len, int initialValue)
    {
        int arr[] = new int[len];

        for(int i = 0; i < len; i++) {
            arr[i] = initialValue;
        }

        return arr;
    }

    private static boolean checkBalance()
    {
        int arr[] = {2, 2, 2, 1, 2, 2, 10, 1};
        
        int j = arr.length - 1;
        int i = 0;
        int sumL = arr[i];
        int sumR = arr[j];

        for(; i < arr.length;) {

            if(sumL < sumR)
            {
                i++;
                sumL += arr[i];
            }
            if(sumL > sumR)
            {
                j--;
                sumR += arr[j];
            }
            if(sumL == sumR) {
                return true;
            }
        }

        return false;
    }




}
